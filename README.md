# Single page test project by Daniel Keeffe

## Disclaimer
**This project has been designed to create the perfect reproduction of the designs included by hj-test**.

This project took a total production time of 18 hours not including breaks this is based on actual coding time.

Whenever possible I would spend as much time as I could on this project, to create the  pixel perfect version of the previews located in the design folder that is my goal.

This project would of been live a lot sooner if I didnt have so many responsibilities such as work and a busy home life, I rarely get the time to have a progressive coding session, to create the best code I can so I came up with this:


## What did I do?
I used vw instead of px for font-size
I used vw instead of px for images
I used vw instead of px for divs
I used vw instead of px for font-awesome
why?
Because you can zoom in/ out and the page, it will stay the same size, this is great for phones tablets and pcs.

Give this project a try:
'http://193.150.14.109/UKFAST/'

After i managed to get everything to scale properly I fixed the menu so it would not move, I set everything below the menu to absolute so when i scroll the other content follows, this part itself was sort of fun to make.

##Changes to repo:
- Javascript is Removed.
- Fonts folder removed due to own ones being used.
- Changed images to webp for faster page load times with a fallback for non supported browsers.
- I completely removed the old SCSS folders and used my own custom SCSS folder names to give this project more personality.



## Installation
1. Clone repo
2. Run `npm install` to install dependencies
3. Run `gulp` to initiate local server and watch for changes
4. CSS and JS are edited in '/src', then compiled with gulp to '/dist'

*You can run `gulp build` to create assets without server*

## Production Build
1. Run `gulp build --production` to disable sourcemaps and add minification


##In closing
As I wish I could of made this preview into a great website, I think the time has come to showcase a production ready working sample of my works, I am sorry I did not get to finish it, I hope you find this project inspiring and learned something.

This has been fun and I would love to do more, regards Daniel.

